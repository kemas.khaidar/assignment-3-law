from background_task import background
from functools import partial
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import math
import os
import pika
import time
import threading
import types
import zipfile

@csrf_exempt
def index(request):
    response = {}
    try:
        if 'X-ROUTING-KEY' in request.headers:
            file = request.FILES['file']
            fileName = request.FILES['file'].name
            handle_uploaded_file(file, fileName)
            routing_key = request.headers.get('X-ROUTING-KEY')
            compressFileRequest(routing_key, fileName)
            response['success'] = "True"
            return JsonResponse(response)
        else:
            response['success'] = "False"
    except Exception as e:
        response['success'] = "False"
    finally:
        return JsonResponse(response)

@background(schedule=1)
def compressFileRequest(routing_key, fileName):
    out_file = "out.bz2"
    progress.bytes = 0
    progress.obytes = 0
    with zipfile.ZipFile(out_file, 'w', compression=zipfile.ZIP_DEFLATED) as myzip:
        myzip.fp.write = types.MethodType(partial(progress, routing_key, os.path.getsize(fileName), myzip.fp.write), myzip.fp)
        myzip.write(fileName)
    myzip.close()
    os.remove(fileName)
    time.sleep(0.25)
    sendMessage(routing_key, "File compressed successfully!")

def handle_uploaded_file(f, name):
    with open(name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def progress(routing_key, total_size, original_write, self, buf):
    progress.bytes += len(buf)
    progress.obytes += 1024 * 8  # Hardcoded in zipfile.write
    print("{} bytes written".format(progress.bytes))
    print("{} original bytes handled".format(progress.obytes))
    percentage = int(100 * progress.obytes / total_size)
    print("{} % done".format(percentage))
    message = "Compression progress: " + str(roundDown(percentage)) + "%"
    sendMessage(routing_key, message)
    return original_write(buf)

def roundDown(x):
    result = int(math.floor(x / 10.0)) * 10
    if result > 100:
        return 100
    else:
        return result 

def sendMessage(routing_key, message):
    credentials = pika.PlainCredentials('0806444524', '0806444524')
    connectionParameters = pika.ConnectionParameters('152.118.148.95', 5672, '/0806444524', credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange='1606831035', exchange_type='direct')
    channel.queue_declare(queue=routing_key)
    channel.basic_publish(exchange='1606831035', routing_key=routing_key, body=message)
    print(" [x] Sent %r:%r" % (routing_key, message))
    connection.close()