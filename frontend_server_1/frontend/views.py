from django.shortcuts import render
from .forms import FileForm

import json
import os
import string
import random
import requests

def index(request):
    response = {}
    fileForm = FileForm()
    response['form'] = fileForm
    return render(request, 'base.html', response)

def upload(request):
    response = {}
    if request.POST:
        fileForm = FileForm(request.POST, request.FILES)
        if fileForm.is_valid:
            routingKey = generateRoutingKey()
            response['routingKey'] = routingKey
            response['success'] = handle_uploaded_file(request, routingKey)
    fileForm = FileForm()
    response['form'] = fileForm
    return render(request, 'base.html', response)

def handle_uploaded_file(request, routingKey):
    file = request.FILES['file']
    fileName = request.FILES['file'].name
    with open(fileName, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    url = 'http://152.118.148.95:20456/index/'
    headers = {'X-ROUTING-KEY': routingKey}
    files = {'file': open(fileName,'rb')}
    r = requests.post(url, headers=headers, files=files)
    os.remove(fileName)
    return r.json().get('success')

def generateRoutingKey(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))